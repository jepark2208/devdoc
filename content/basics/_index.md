---
title: Basics
weight: 5
pre: "<b>1. </b>"
chapter: true
---

# Page Create

## Category, Article Page Create

- content 폴더 하위에 __폴더를 만들고__ `_index.md`(required) __파일을 만든다.__ __('_'가 없으면 페이지 생성 안됨)__
- 카테고리 폴더를 만들고 난 후, 카테고리 하위 폴더에 게시글 폴더를 만들면 된다.
- 이미지를 불러올 때는 __images 폴더를 만들어서 사용 권장__

## Category, Article Page (in index.md)

```
    ---
    date: 2016-04-09T16:50:16+02:00 // 작성 날짜
    title: Basics // 왼쪽 카테고리 명칭 명
    weight: 5 // 카테고리 oder 가중치
    pre: "<b>1. </b>" // 왼쪽 카테고리 번호 매핑
    chapter: true // css class -> true 이면 content 가운데 정렬
    ---
```

    content
    ├── level-one
    │   ├── level-two
    │   │   ├── _index.md               <-- /level-one/level-two
    │   │   ├── images                  <-- /level-one/images/
    │   ├── _index.md                   <-- /level-one
    │   ├── images                      <-- /level-one/images
    ├── images                          <-- /images
    ├── _index.md                       <-- /